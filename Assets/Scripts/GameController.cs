using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// ★追加
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    // ★追加
    public Text timeLabel;
    public float timeCount;

    //コインアイコン
    //配列（複数のデータを入れることのできるしきりにある箱)
    public GameObject[] coinIcons;
    private Ball ballScript;
    private int getCoin;

    void Start()
    {
        // ★追加
        timeLabel.text = "TIME;" + timeCount;

        //コインアイコン
        //BallオブジェクトについてくるBallスクリプトの情報を取得
        ballScript = GameObject.Find("Ball").GetComponent<Ball>();
    }

    void Update()
    {
        
        timeCount -= Time.deltaTime;

        // ToString("0")は小数点を切り捨て
        // 小数点1位まで表示するにはToString("n1")
        // 小数点2位まで表示するにはToString("n2")
        timeLabel.text = "TIME;" + timeCount.ToString("0");

        if (timeCount < 0)
        {
            SceneManager.LoadScene("GameOver");
        }

        //コインアイコン
        //BallScript内にあるcoinCountの情報を取得
        getCoin = ballScript.coinCount;

        for(int i = 0; i < coinIcons.Length; i++)
        {
            if(getCoin <= i)
            {
                //コインアイコンを表示状態にする
                coinIcons[i].SetActive(true);
            }
            else
            {
                //コインアイコンを非表示状態にする
                coinIcons[i].SetActive(false);
            }
        }
    }
}