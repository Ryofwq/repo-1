using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneScript : MonoBehaviour
{
    public void changeScene()
    {
        //現在のシーンの名前を取得
        string sceneName = SceneManager.GetActiveScene().name;

        if (sceneName == "NextStage")
        {
            SceneManager.LoadScene("Stage2");
        }
    }
}
