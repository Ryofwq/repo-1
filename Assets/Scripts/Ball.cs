using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ★追加
using UnityEngine.SceneManagement;

public class Ball : MonoBehaviour
{
    //「変数」の定義(まずは変数という単語を頭にインプットする)
    public float moveSpeed;
    private Rigidbody rb;

    //効果音追加
    public AudioClip coinGet;
    
    //ジャンプ追加
    public float jumpSpeed;

    //空中でのジャンプを禁止する
    private bool isJumping = false;

    //コインをとった枚数をカウント //privateをpublicに変更
    public int coinCount = 0;

    //コインを一定数とると出現する
    public GameObject moveFloor;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float moveH = Input.GetAxis("Horizontal");
        float moveV = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveH, 0, moveV);
        rb.AddForce(movement * moveSpeed);

        // ★追加（ジャンプ）
        //改良して空中でのジャンプを禁止
        if (Input.GetButtonDown("Jump") && isJumping == false)
        {
            rb.velocity = Vector3.up * jumpSpeed;

            isJumping = true;
        }
    }

    //コインにぶつかったらコインが消える
    private void OnTriggerEnter(Collider other)
    {
        //ぶつかった相手に「Coin」という「Tag」がついていたならば(条件)
        if (other.CompareTag("Coin"))
        {
            //ぶつかった相手を破壊(実行)
            Destroy(other.gameObject);

            //Coinを取った時の効果音
            AudioSource.PlayClipAtPoint(coinGet, transform.position);

            //コインを取得するごとに「coinCount」を1つずつ追加
            coinCount += 1;

            // もしも「coinCount」が２になったら（条件）
            if (coinCount == 5)
            {
                // GameClearシーンに遷移する。
                // 遷移させるシーンは「名前」で特定するので合致させること（ポイント）
                SceneManager.LoadScene("GameClear");
            }

            // ★追加
            if (coinCount == 2)
            {
                // （ポイント）
                // SetActive()メソッドはオブジェクトをアクティブ／非アクティブ状態にすることができる。
                moveFloor.SetActive(true);
            }

            if (coinCount == 3)
            {
                SceneManager.LoadScene("GameClear");
            }
        }
    }

    //ジャンプを復活させる
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            isJumping = false;
        }
    }
}
