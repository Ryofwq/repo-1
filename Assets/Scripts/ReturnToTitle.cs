using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class ReturnToTitle : MonoBehaviour
{
    public int timeCount;

    void Start()
    {
        Invoke("GoTitle", timeCount);
    }

    void GoTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
