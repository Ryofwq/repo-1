using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWall : MonoBehaviour
{
    public float speed;
    private Vector3 pos;
    private bool isStop = false;

    void Start()
    {
        pos = transform.position;
    }

    void Update()
    {
        if (isStop == false)
        {
            pos.x += Time.deltaTime * speed; // speedは移動速度
            transform.position = pos;

            if (pos.x > 3) // 終点（自由に変更可能）
            {
                isStop = true;
            }
        }
        else if (isStop == true)
        {
            pos.x -= Time.deltaTime * speed;
            transform.position = pos;

            if (pos.x < 3) // 始点（自由に変更可能）
            {
                isStop = false;
            }
        }
    }
}